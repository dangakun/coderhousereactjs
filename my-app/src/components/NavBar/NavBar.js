import './NavBar.css';
import logo from '../../assets/images/logo.svg';

function NavBar() {
	return (
		<nav class="topnav">
			<img alt="logo" className="logo" src={logo} />
			<ul>
				<li><a href="#section1">Seccion 1</a></li>
				<li><a href="#section2">Seccion 2</a></li>
			</ul>
		</nav>
	);
}
export default NavBar;
