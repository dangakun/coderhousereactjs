import './Main.css';
import NavBar from '../NavBar/NavBar';
import {numberWithCommas} from '../../utils/string';

function Main() {
  return (
    <div className="App">
      <NavBar />
      <div className="container">
        {`numero de ejemplo: ${numberWithCommas(123456)}`}
      </div>
    </div>
  );
}

export default Main;
